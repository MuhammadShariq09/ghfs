@extends('layouts.app')
@section('title','Add Challenge')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('content')
@section('css')
@endsection

<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-body">
         <!-- Basic form layout section start -->
         <section id="combination-charts">
            <div class="row">
               <div class="col-12">
                  <div class="card rounded p-3 admin-overview-main">
                     <div class="row">
                        <div class="col-lg-12">
                           <h1>FFTP Add Challenges</h1>
                           <div class="row">
                              <div class="col-md-6 col-sm-12">
                               <div class="admin-top-red-nav">
                                    <ul>
                                       <li>
                                          <input type="radio" checked style="display:none" name="path" id="East">
                                          <label for="East">East Path</label>
                                       </li>
                                       <li>
                                          <input type="radio" style="display:none" name="path" id="West">
                                          <label for="West">West Path</label>
                                       </li>
                                       <li>
                                          <input type="radio" style="display:none" name="path" id="North">
                                          <label for="North">North Path</label>
                                       </li>
                                       <li>
                                          <input type="radio" style="display:none" name="path" id="South">
                                          <label for="South">South Path</label>
                                       </li>
                                    </ul>
                                 </div>
                                 <!--admin top red nav-->
                                 <div class="clearfix"></div>
                              </div>
                              <div class="col-md-6 col-sm-12 mt-2">
                                 <a href="#" data-toggle="modal" data-target="#default2" class="green-btn-project2">Challenges Detail</a>
                                 <a href="#" class="fit-setting-a"><i class="fa fa-cog" aria-hidden="true"></i></a>
                              </div>
                           </div>
                           <div class="admin-top-blue-nav">
                              <ul>
                                 <li>
                                    <input type="radio" checked style="display:none" name="level_no" id="Level_1">
                                    <label for="Level_1">Level 1</label>
                                 </li>
                                 <li>
                                    <input type="radio"  style="display:none" name="level_no" id="Level_2">
                                    <label for="Level_2">Level 2</label>
                                 </li>
                                 <li>
                                    <input type="radio"  style="display:none" name="level_no" id="Level_3">
                                    <label for="Level_3">Level 3</label>
                                 </li>
                                 <li>
                                    <input type="radio"  style="display:none" name="level_no" id="Level_4">
                                    <label for="Level_4">Level 4</label>
                                 </li>
                                 <li>
                                    <input type="radio"  style="display:none" name="level_no" id="Level_5">
                                    <label for="Level_5">Level 5</label>
                                 </li>
                                 <li>
                                    <input type="radio"  style="display:none" name="level_no" id="Level_6">
                                    <label for="Level_6">Level 6</label>
                                 </li>
                                 <li>
                                    <input type="radio"  style="display:none" name="level_no" id="Level_7">
                                    <label for="Level_7">Level 7</label>
                                 </li>
                                 <li>
                                    <input type="radio"  style="display:none" name="level_no" id="Level_8">
                                    <label for="Level_8">Level 8</label>
                                 </li>
                                 <li>
                                    <input type="radio"  style="display:none" name="level_no" id="Level_9">
                                    <label for="Level_9">Level 9</label>
                                 </li>
                                 <li>
                                    <input type="radio"  style="display:none" name="level_no" id="Level_10">
                                    <label for="Level_9">Level 10</label>
                                 </li>
                              </ul>
                           </div>
                           <!--admin top blue nav en d-->
                           <div class="clearfix"></div>
                           <div class="row">
                              <div class="col-lg-2 col-md-2 col-sm-12">
                                 <button name="file" class="upload-admin-img" 
                                 onclick="console.log($(this).next('input').click())">
                                 <img src="{{asset('images/upload-img_03.jpg')}}" class="img-fluid" alt="">   
                                 </button>
                                 <input type="file" onchange="readURL(this)" style="display:none;" name="file">
                              </div>
                              <!--col end-->
                              <div class="col-lg-10 col-md-10 col-sm-12">
                                 <div class="admin-add-user-main pt-0">
                                    <h3>New FTFP Challenge </h3>
                                    <div class="mb-2">
                                       <label>Challenge Title :</label>
                                       <input type="text" class="admin-chlng-fit-top-input">
                                    </div>
                                    <div class="mb-2">
                                       <label>Reward Points: </label>
                                       <input type="text" class="admin-chlng-fit-top-input">
                                    </div>
                                    <div class="mb-2">
                                       <label>Description:</label>
                                       <textarea class="admin-chlng-fit-top-txtara"></textarea>
                                    </div>
                                 </div>
                              </div>
                              <!--col end-->
                           </div>
                           <!--row end-->

                           <div class="">
                           <div class="file-repeater">
                                <div data-repeater-list="repeater-list">
                                    <div data-repeater-item>
                                          <div class="row">
                                          <div class="col-lg-2 col-md-2 col-sm-12">
                                             <button name="file" class="upload-admin-img" 
                                             onclick="$(this).next('input').click()">
                                             <img src="{{asset('images/upload-img_03.jpg')}}" class="img-fluid" alt="">   
                                             </button>
                                             <input type="file" onchange="readURL(this)" style="display:none;" name="exe_image">
                                          </div>
                                             <!--col end-->
                                             <div class="col-lg-10 col-md-10 col-sm-12">
                                                <div class="admin-add-user-main pt-0">
                                                   <button data-repeater-delete type="button" class="pull-right ml-3 fftp-add-more-btn"><i class="fa fa-minus-circle"></i> Remove exercise </button>
                                                   <h3>Exercise Details:</h3>
                                                   <div class="row">
                                                      <div class="col-lg-6 col-md-12">
                                                         <div class="mb-2">
                                                            <label>Exercise Title :</label>
                                                            <input type="text" class="admin-chlng-fit-top-input">
                                                         </div>
                                                         <div class="mb-2">
                                                            <label>Sets :</label>
                                                            <input type="text" class="admin-chlng-fit-top-input">
                                                         </div>
                                                         <div class="mb-2">
                                                            <label>Reps :</label>
                                                            <input type="text" class="admin-chlng-fit-top-input">
                                                         </div>
                                                         <div class="mb-2">
                                                            <label>Category :</label>
                                                            <select class="admin-chlng-fit-top-input">
                                                               @foreach($Categories as $cat)
                                                               <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                               @endforeach
                                                            </select>
                                                         </div>
                                                      </div>
                                                      <!--col end-->
                                                      <div class="col-lg-6 col-md-12">
                                                         <div class="mb-2">
                                                            <label>Description:</label>
                                                            <textarea class="admin-chlng-fit-top-txtara2"></textarea>
                                                         </div>
                                                      </div>
                                                      <!--col end-->
                                                   </div>
                                                   <!--row end-->
                                                   <!-- <button class="cnt-btnn2">Create Challenge </button> -->
                                                </div>
                                             </div>
                                          </div>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-12"></div>
                                    <div class="col-lg-10 col-md-10 col-sm-12">
                                       <button data-repeater-create type="button" class="ml-3 fftp-add-more-btn"><i class="fa fa-plus-circle"></i> Add more exercise </button>
                                    </div>
                                 </div>
                            </div>                              
                           </div>
                           <!--row end-->

                        </div>
                        <button class="cnt-btnn2">Create Challenge </button>

                        <!--col  end-->
                     </div>
                     <!--row end-->
                  </div>
               </div>
            </div>
         </section>
         <!-- // Basic form layout section end -->
      </div>
   </div>
</div>



@endsection
@section('js')
<script>
! function(e, t, r) {
    "use strict";
    r(".repeater-default").repeater(), r(".file-repeater, .contact-repeater").repeater({
        show: function() {
            r(this).slideDown()
        },
        hide: function(e) {
            confirm("Are you sure you want to remove this item?") && r(this).slideUp(e)
        }
    })
}(window, document, jQuery);

function readURL(input, target) {
   var this_ = input;
   if (input.files && input.files[0]) {
         var reader = new FileReader();
         reader.onload = function(e) {
            $(this_).prev('button').find('img').attr('src', e.target.result);
            $(this).next('img').attr('src', e.target.result);
         }
         reader.readAsDataURL(input.files[0]);
   }
}
</script>
@endsection