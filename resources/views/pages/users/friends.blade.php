@extends('layouts.app')
@section('title','User Profile')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('content')
@section('css')
@endsection
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-body">
         <!-- Basic form layout section start -->
         <section id="combination-charts">
            <div class="row">
               <div class="col-12">
                  <div class="card rounded p-3 admin-overview-main">
                     <div class="row">
                        <div class="col-lg-12">
                           <h1>User Profile</h1>
                           <div class="admin-user-profile-top">
                              <div class="row align-items-center">
                                 <div class="col-md-3 col-sm-12">
                                    <div class="admin-user-profile-top-image-main">
                                       <img src="{{ uimgP($user->name, $user->image) }}" alt="" />
                                       <h6>{{$user->name}}</h6>
                                       <span>{{$user->email}}</span>
                                    </div>
                                 </div>
                                 <!--col left end-->
                                 <div class="col-md-7 col-sm-12">
                                    <div class="row">
                                       <div class="col-md-3 col-sm-12">
                                          <div class="admin-user-profile-top-boxs">
                                             <span>Level</span>
                                             <h5>{{$user->stats->level}}</h5>
                                          </div>
                                       </div>
                                       <!--col end-->
                                       <div class="col-md-3 col-sm-12">
                                          <div class="admin-user-profile-top-boxs">
                                             <span>Challenges</span>
                                             <h5>09</h5>
                                          </div>
                                       </div>
                                       <!--col end-->
                                       <div class="col-md-3 col-sm-12">
                                          <div class="admin-user-profile-top-boxs">
                                             <span>Total Points:</span>
                                             <h5>{{$user->stats->points}}</h5>
                                          </div>
                                       </div>
                                       <!--col end-->
                                       <div class="col-md-3 col-sm-12">
                                          <div class="admin-user-profile-top-boxs">
                                             <span>Votes:</span>
                                             <h5>{{$user->stats->challenge_votes}}</h5>
                                          </div>
                                       </div>
                                       <!--col end-->
                                    </div>
                                    <!--row end-->
                                 </div>
                                 <!--col end-->
                                 <div class="col-md-2 col-sm-12">
                                    <div class="admin-user-profile-top-boxs-logo">
                                       <img src="{{asset('images/User-Profile_03.png')}}" alt="" />
                                       <h5>{{$user->stats->path}} Path</h5>
                                    </div>
                                 </div>
                              </div>
                              <!--row end-->
                           </div>
                           <!--admin-user-profile-top end-->
                           <div class="admin-top-red-nav">
                              <ul>
                                 <li><a href="{{URL::current()}}">Details</a></li>
                                 <li><a href="{{URL::current().'?challenge=true'}}">Challenges</a></li>
                                 <li  class="active"><a href="{{URL::current().'?friends=true'}}">Friends</a></li>
                              </ul>
                           </div>
                           <!--admin top red nav-->
                           <div class="clearfix"></div>
                           <div class="row">
                                @foreach($friends as $friend)
                                <div class="col-lg-3 col-md-6 col-sm-12">
                                    <div class="chalng-members-box admin-user-frinds-boxs box-shadow-1">
                                        <img src="{{ uimgP($friend->name, $friend->image) }}" alt="">
                                        <div class="flex-column">
                                        <p>{{$friend->name}}</p>
                                        <span>Level: {{$friend->stats->level}}</span>
                                        <span>Path: {{$friend->stats->path}}</span>
                                        </div>
                                    </div>
                                </div><!--col end-->
                                @endforeach                                 
                                                              
                            </div>
                        </div>
                        <!--col  end-->
                     </div>
                     <!--row end-->
                  </div>
               </div>
            </div>
         </section>
         <!-- // Basic form layout section end -->
      </div>
   </div>
</div>

@endsection
@section('js')

@endsection