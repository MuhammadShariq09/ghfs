@extends('layouts.app')
@section('title','User Profile')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('content')
@section('css')
@endsection
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-body">
         <!-- Basic form layout section start -->
         <section id="combination-charts">
            <div class="row">
               <div class="col-12">
                  <div class="card rounded p-3 admin-overview-main">
                     <div class="row">
                        <div class="col-lg-12">
                           <h1>User Profile</h1>
                           <div class="admin-user-profile-top">
                              <div class="row align-items-center">
                                 <div class="col-md-3 col-sm-12">
                                    <div class="admin-user-profile-top-image-main">
                                       <img src="{{ uimgP($user->name, $user->image) }}" alt="" />
                                       <h6>{{$user->name}}</h6>
                                       <span>{{$user->email}}</span>
                                    </div>
                                 </div>
                                 <!--col left end-->
                                 <div class="col-md-7 col-sm-12">
                                    <div class="row">
                                       <div class="col-md-3 col-sm-12">
                                          <div class="admin-user-profile-top-boxs">
                                             <span>Level</span>
                                             <h5>{{$user->stats->level}}</h5>
                                          </div>
                                       </div>
                                       <!--col end-->
                                       <div class="col-md-3 col-sm-12">
                                          <div class="admin-user-profile-top-boxs">
                                             <span>Challenges</span>
                                             <h5>09</h5>
                                          </div>
                                       </div>
                                       <!--col end-->
                                       <div class="col-md-3 col-sm-12">
                                          <div class="admin-user-profile-top-boxs">
                                             <span>Total Points:</span>
                                             <h5>{{$user->stats->points}}</h5>
                                          </div>
                                       </div>
                                       <!--col end-->
                                       <div class="col-md-3 col-sm-12">
                                          <div class="admin-user-profile-top-boxs">
                                             <span>Votes:</span>
                                             <h5>{{$user->stats->challenge_votes}}</h5>
                                          </div>
                                       </div>
                                       <!--col end-->
                                    </div>
                                    <!--row end-->
                                 </div>
                                 <!--col end-->
                                 <div class="col-md-2 col-sm-12">
                                    <div class="admin-user-profile-top-boxs-logo">
                                       <img src="{{asset('images/User-Profile_03.png')}}" alt="" />
                                       <h5>{{$user->stats->path}} Path</h5>
                                    </div>
                                 </div>
                              </div>
                              <!--row end-->
                           </div>
                           <!--admin-user-profile-top end-->
                           <div class="admin-top-red-nav">
                              <ul>
                                 <li class="active"><a href="{{URL::current()}}">Details</a></li>
                                 <li><a href="{{URL::current().'?challenge=true'}}">Challenges</a></li>
                                 <li><a href="{{URL::current().'?friends=true'}}">Friends</a></li>
                              </ul>
                           </div>
                           <!--admin top red nav-->
                           <div class="clearfix"></div>
                           <div class="row">
                              <div class="col-xl-6 col-lg-6 col-md-12">
                                 <div class="row user-profille-inner-row">
                                    <div class="col-md-5 col-sm-12">
                                       <p><i class="fa fa-user-circle" aria-hidden="true"></i> User ID: </p>
                                    </div>
                                    <div class="col-md-7 col-sm-12"><span>{{$user->uuid}}</span></div>
                                 </div>
                                 <!--inner row end-->
                                 <div class="row user-profille-inner-row">
                                    <div class="col-md-5 col-sm-12">
                                       <p><i class="fa fa-envelope" aria-hidden="true"></i> Email: </p>
                                    </div>
                                    <div class="col-md-7 col-sm-12"><span>{{$user->email}}</span></div>
                                 </div>
                                 <div class="row user-profille-inner-row">
                                    <div class="col-md-5 col-sm-12">
                                       <p><i class="fa fa-map-marker" aria-hidden="true"></i> Address: </p>
                                    </div>
                                    <div class="col-md-7 col-sm-12"><span>{{$user->address}}</span></div>
                                 </div>
                                 <div class="row user-profille-inner-row">
                                    <div class="col-md-5 col-sm-12">
                                       <p><i class="fa fa-map-marker" aria-hidden="true"></i> Country: </p>
                                    </div>
                                    <div class="col-md-7 col-sm-12"><span>{{$user->country}}</span></div>
                                 </div>
                                 <div class="row user-profille-inner-row">
                                    <div class="col-md-5 col-sm-12">
                                       <p><i class="fa fa-map-marker" aria-hidden="true"></i> State: </p>
                                    </div>
                                    <div class="col-md-7 col-sm-12"><span>{{$user->state}}</span></div>
                                 </div>
                                 <div class="row user-profille-inner-row">
                                    <div class="col-md-5 col-sm-12">
                                       <p><i class="fa fa-map-marker" aria-hidden="true"></i> City: </p>
                                    </div>
                                    <div class="col-md-7 col-sm-12"><span>{{$user->city}}</span></div>
                                 </div>
                                 <div class="row user-profille-inner-row">
                                    <div class="col-md-5 col-sm-12">
                                       <p><i class="fa fa-map-marker" aria-hidden="true"></i> Postal Code: </p>
                                    </div>
                                    <div class="col-md-7 col-sm-12"><span>{{$user->zipcode}}</span></div>
                                 </div>


                                 <div class="row user-profille-inner-row">
                                    <div class="col-md-5 col-sm-12">
                                       <p><i class="fa fa-phone" aria-hidden="true"></i> Phone: </p>
                                    </div>
                                    <div class="col-md-7 col-sm-12"><span>{{$user->contact}}</span></div>
                                 </div>
                                 <!--inner row end-->
                                 <div class="row user-profille-inner-row">
                                    <div class="col-md-5 col-sm-12">
                                       <p><i class="fa fa-user" aria-hidden="true"></i> Member Since: </p>
                                    </div>
                                    <div class="col-md-7 col-sm-12"><span>{{$user->created_at}}</span></div>
                                 </div>
                                 <!--inner row end-->
                              </div>
                              <!--left col main end-->
                              <div class="col-xl-6 col-lg-6 col-md-12">
                              </div>
                              <!--left col end-->
                           </div>
                        </div>
                        <!--col  end-->
                     </div>
                     <!--row end-->
                  </div>
               </div>
            </div>
         </section>
         <!-- // Basic form layout section end -->
      </div>
   </div>
</div>

@endsection
@section('js')

@endsection