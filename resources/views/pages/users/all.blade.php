@extends('layouts.app')
@section('title','All Users')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('content')
@section('css')
@endsection
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-body">
         <!-- Basic form layout section start -->
         <section id="combination-charts">
            <div class="row">
               <div class="col-12">
                  <div class="card rounded p-3 admin-overview-main">
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="row">
                              <div class="col-md-6 col-sm-12">
                                 <h1>All Users</h1>
                              </div>
                              <div class="col-md-6 col-sm-12">
                                 <a href="{{url('admin/users/add')}}" class="green-btn-project"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Users</a>
                              </div>
                           </div>
                           <div class="maain-tabble table-responsive">
                              <table class="table table-striped table-bordered zero-configuration">
                                 <thead>
                                    <tr>
                                       <th>SN.o</th>
                                       <th>User</th>
                                       <th>Email</th>
                                       <th>Contact Number</th>
                                       <th>Registered On</th>
                                       <th>Path</th>
                                       <th>Level</th>
                                       <th>Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($users as $index => $user)
                                    <tr>
                                       <td>{{++$index}}</td>
                                       <td> {!! uimg($user->name, $user->image) !!} {{$user->name}}</td>
                                       <td>{{ $user->email }}</td>
                                       <td>{{ $user->contact }}</td>
                                       <td>{{ $user->created_at }}</td>
                                       <td>{{ $user->stats->path }}</td>
                                       <td>{{ $user->stats->level }}</td>
                                       <td>{{ ($user->status) ? "InActive" : "Active"}}</td>
                                       <td>
                                          <div class="btn-group mr-1 mb-1">
                                             <button type="button" class="btn dropdown-toggle btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                                             <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                <a class="dropdown-item" href="{{url('admin/users/view/'.$user->uuid)}}"><i class="fa fa-eye"></i>View</a>
                                                <a class="dropdown-item" href="#"><i class="fa fa-ban"></i>{{ ($user->status) ? "UnBlocked User" : "Block User"}}</a>
                                             </div>
                                          </div>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <!--col  end-->
                     </div>
                     <!--row end-->
                  </div>
               </div>
            </div>
         </section>
         <!-- // Basic form layout section end -->
      </div>
   </div>
</div>


@endsection
@section('js')

@endsection