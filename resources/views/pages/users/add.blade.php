@extends('layouts.app')
@section('title','Add User')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('content')

@section('css')
<link rel='stylesheet' href='https://foliotek.github.io/Croppie/croppie.css'>
<style>
    label i.fa {
        padding-right: 8px;
    }
</style>
@endsection
    
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-body">
         <section id="combination-charts">
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-2 col-sm-12"></div>
                     <div class="col-md-8 col-sm-12">
                        <div class="card rounded p-3 admin-overview-main">
                           <div class="row">
                              <div class="col-lg-12">
                                 <form action="{{ url('admin/users/add') }}" method="post" enctype="multipart/form-data">
                                 @csrf
                                 <div class="admin-add-user-main">
                                 @if(Session::has('message'))
                                    <div class="alert alert-success">
                                        <strong>{{ Session::get('message')  }}</strong>
                                    </div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="alert alert-danger">
                                        <strong>{{ Session::get('error')  }}</strong>
                                    </div>
                                @endif

                                  <h1><a href="#" class="profile-back"><i class="fa fa-angle-left" aria-hidden="true"></i></a> Add User</h1>
                                    <div class="regi-profile-main">
                                    <img src="{{ asset('/images/upload-img.jpg') }}" id="personal_img" alt="">
                                    <button type="button" style="margin-top: 0;" name="file"  class="uplrd-img-btn"
                                     onclick="document.getElementById('upload').click()"><i class="fa fa-camera" aria-hidden="true"></i>
                                    </button>
                                    <input type="file"  accept="image/*" onchange="readURL(this, 'personal_img')" id="upload" name="personal_img">         
                                    <input type="hidden" id="personal_img_base64" name="personal_img_base64">       
                                    </div>
                                    <div class="mb-2">
                                       <label><i class="fa fa-user-circle"></i>Name *</label>
                                       <input  type="text" value="{{old('name')}}" id="name" class="admin-chlng-fit-top-input"
                                             placeholder="" name="name">
                                    </div>
                                    <div class="mb-2">
                                       <label><i class="fa fa-envelope"></i>Email Address *</label>
                                       <input type="email" value="{{old('email')}}" id="email"
                                                         class="admin-chlng-fit-top-input" placeholder="" name="email">
                                    </div>
                                    <!--pro inner row end-->
                                    <div class="mb-2">
                                       <label><i class="fa fa-key"></i>Enter Password *</label>
                                       <input  type="password" id="password" class="admin-chlng-fit-top-input" placeholder=""
                                                         name="password">
                                    </div>
                                    <div class="mb-2">
                                       <label><i class="fa fa-map-marker"></i>Address</label>
                                       <input  type="text" value="{{old('address')}}" id="autocomplete" onfocus="geolocate()"
                                                         class="admin-chlng-fit-top-input" placeholder="Enter your address"
                                                         name="address">
                                    </div>
                                    <!--pro inner row end-->
                                    <div class="mb-2">
                                       <label><i class="fa fa-map-marker"></i>Country</label>
                                       <input  type="text" value="{{old('country')}}" id="country" class="admin-chlng-fit-top-input"
                                                         placeholder="" name="country">
                                    </div>

                                    <div class="mb-2">
                                       <label><i class="fa fa-map-marker"></i>State</label>
                                       <input  type="text" value="{{old('state')}}" id="administrative_area_level_1" class="admin-chlng-fit-top-input"
                                                         placeholder="" name="state">
                                    </div>
                                    <!--pro inner row end-->
                                    <div class="mb-2">
                                       <label><i class="fa fa-map-marker"></i>City </label>
                                       <input  type="text" value="{{ old('city') }}" id="locality" class="admin-chlng-fit-top-input"
                                                         placeholder="" name="city">
                                    </div>
                                    <div class="mb-2">
                                       <label><i class="fa fa-map-marker"></i>Zip Code </label>
                                       <input  type="text" value="{{ old('zip_code') }}" id="postal_code" class="admin-chlng-fit-top-input"
                                                         placeholder="" name="zipcode">
                                    </div>
                                    <div class="mb-2">
                                       <label><i class="fa fa-phone"></i>Contact number:</label>
                                       <input  type="number" id="number" value="" class="admin-chlng-fit-top-input"
                                                         placeholder="{{ old('contact') }}" name="contact">
                                    </div>
                                    <button  type="submit" class="save cnt-btnn" >Add User</button>
                                 </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-2 col-sm-12"></div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>


<div class="modal" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <!--<div class="modal-body" style="min-height:400px">-->
         <div class="modal-body" style="">
            <div id="upload-demo" style="width:350px"></div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" id="cropImageBtn" class="btn btn-primary">Save</button>
         </div>
      </div>
   </div>
</div>

@endsection
@section('js')
    <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
    <script type="text/javascript">
        var $uploadCrop,
            tempFilename,
            rawImg,
            imageId;
    
        $uploadCrop = $('#upload-demo').croppie({
          enableExif: true,
          viewport: {
            width: 200,
            height: 200,
            type: 'circle'
          },
          boundary: {
            width: 250,
            height: 250
          }
        });    
        $('#upload').on('change', function(){
            $('#cropImagePop').modal('toggle');
            var reader = new FileReader();
            reader.onload = function(e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function() {
                    console.log('jQuery bind complete');
                });
            }
            reader.readAsDataURL(this.files[0]);
        });
        
        $('#cropImageBtn').on('click', function(ev) {
            $uploadCrop.croppie('result', {
                type: 'base64',
                format: 'jpeg',
                size: { width: 300, height: 300 }
            }).then(function(resp) {
                $('#personal_img').attr('src', resp);
                $('#personal_img_base64').val(resp);
                $('#cropImagePop').modal('hide');
            });
        });



        function readURL(input, target) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#'+target).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
@endsection

