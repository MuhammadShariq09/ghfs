@extends('layouts.app')
@section('title','Settings')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('content')
@section('css')
@endsection

<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-body">
         <section id="combination-charts">
            <div class="row">
               <div class="col-12">
                  <div class="card rounded p-3 admin-overview-main">
                     <div class="row">
                        <div class="col-lg-12">
                           @if(Session::has('message'))
                              <div class="alert alert-success">
                                    <strong>{{ Session::get('message')  }}</strong>
                              </div>
                           @endif
                           @if(Session::has('error'))
                              <div class="alert alert-danger">
                                    <strong>{{ Session::get('error')  }}</strong>
                              </div>
                           @endif
                           <h1>FTFP Settings </h1>
                           <div class="admin-chlng-setting">
                              <form method="post" action="{{url('admin/setting/save')}}">
                              @csrf
                              @foreach($settings as $setting)
                              <div class="row align-items-center">
                                 <div class="col-md-5 col-sm-12"><label>{{$setting->text}}</label></div>
                                 <div class="col-md-7 col-sm-12">
                                    <input type="{{ ($setting->type == 'N') ? 'number' : 'text'  }}" required name="{{$setting->key}}" value="{{$setting->value}}" class="admin-chlng-setting-input">
                                    <span>{{$setting->pretext}}</span></div>
                              </div>
                              @endforeach
                              <button type="submit" class="cnt-btnn mt-5">Save Changes</button>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>

@endsection
@section('js')

@endsection