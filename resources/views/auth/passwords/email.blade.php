@extends('layouts.app')
@section('title','Reset Account')
@section('body-class','bg-full-screen-image  pace-done')
@section('content')

<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <section class="flexbox-container">
          <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-lg-6 col-xl-4 col-md-10 col-10 box-shadow-2 p-0">
              <div class="card rad border-grey border-lighten-3 p-5  px-1 py-1 m-0">
                <div class="card-header border-0">
                  <div class="card-title text-center">
                    <img src="{{asset('images/login-logo.png')}}" class="img-fluid" alt="branding logo">
                  </div>
                  
                </div>
                <div class="card-content logn-form">
                
                  <div class="card-body">
                     <h1>Reset Your Account</h1>
                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                    @csrf
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($errors->has('email'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif
                      <fieldset class="form-group position-relative has-icon-left">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        <div class="form-control-position">
                          <i class="ft-user"></i>
                        </div>
                      </fieldset>
                      <button type="submit" class="btn btn-outline-primary btn-block">  {{ __('Send Password Reset Link') }}</button>
                    </form>
                  </div>
                  <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2">
                    <span>or</span>
                  </p>
                  <div class="card-body">
                    <a href="{{route('login')}}" class="btn btn-outline-danger btn-block"> Login</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>


@endsection

