@extends('layouts.app')
@section('title','Register')
@section('body-class','bg-full-screen-image  pace-done')
@section('content')

<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <section class="flexbox-container">
          <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-lg-6 col-xl-4 col-md-10 col-10 box-shadow-2 p-0">
              <div class="card rad border-grey border-lighten-3 p-5  px-1 py-1 m-0">
                <div class="card-header border-0">
                  <div class="card-title text-center">
                  <img src="{{asset('images/login-logo.png')}}" class="img-fluid" alt="branding logo">
                  </div>
                  
                </div>
                <div class="card-content logn-form">
                
                  <div class="card-body">
                    <h1>Register Your Account</h1>
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    @csrf
                    @if ($errors->has('name'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </div>
                    @elseif ($errors->has('email'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @elseif ($errors->has('password'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif
                      <fieldset class="form-group position-relative has-icon-left">
                        <input type="text" class="form-control"
                        class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                        name="name" value="{{ old('name') }}" placeholder="Full Name"
                        required>
                        <div class="form-control-position">
                          <i class="ft-user"></i>
                        </div>
                      </fieldset>
                      <fieldset class="form-group position-relative has-icon-left">
                        <input type="text" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" 
                        required>
                        <div class="form-control-position">
                          <i class="fa fa-envelope-o"></i>
                        </div>
                      </fieldset>
                      <fieldset class="form-group position-relative has-icon-left">
                        <input type="password" class="form-control" name="password" placeholder="Password" 
                        required>
                        <div class="form-control-position">
                          <i class="fa fa-key"></i>
                        </div>
                      </fieldset>
                      <fieldset class="form-group position-relative has-icon-left">
                        <input type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation" 
                        required>
                        <div class="form-control-position">
                          <i class="fa fa-key"></i>
                        </div>
                      </fieldset>                      
                      <button type="submit" class="btn btn-outline-primary btn-block"> Register</button>
                    </form>
                  </div>
                  <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2">
                    <span>Already A User ?</span>
                  </p>
                  <div class="card-body">
                    <a href="{{route('login')}}" class="btn btn-outline-danger btn-block"> Login</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>

@endsection
