@extends('layouts.app')
@section('title','Login')
@section('body-class','bg-full-screen-image  pace-done')
@section('content')

<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <section class="flexbox-container">
          <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-lg-6 col-xl-4 col-md-10 col-10 box-shadow-2 p-0">
              <div class="card rad border-grey border-lighten-3 p-5  px-1 py-1 m-0">
                <div class="card-header border-0">
                  <div class="card-title text-center">
                  <img src="{{asset('images/login-logo.png')}}" class="img-fluid" alt="branding logo">
                  </div>
                  
                </div>
                <div class="card-content logn-form">
                
                  <div class="card-body">
                     <h1>Login to Your Account</h1>
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    @csrf
                    @if ($errors->has('email'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif
                    @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                <strong>{{ session('status') }}</strong>
                            </div>
                    @endif
                    @if ($errors->has('password'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif
                    @if (Session::has('resetPass'))
                            <div class="alert alert-success" role="alert">
                                <strong>{{ Session::get('resetPass') }}</strong>
                            </div>
                    @php Session::forget('resetPass'); @endphp
                    @endif
                      <fieldset class="form-group position-relative has-icon-left">
                        <input type="text" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" 
                        required>
                        <div class="form-control-position">
                          <i class="ft-user"></i>
                        </div>
                      </fieldset>
                      <fieldset class="form-group position-relative has-icon-left">
                        <input type="password" class="form-control" name="password" placeholder="Password" 
                        required>
                        <div class="form-control-position">
                          <i class="fa fa-key"></i>
                        </div>
                      </fieldset>
                      <div class="form-group row">
                        <div class="col-md-12 col-12 float-sm-left text-center text-sm-right">
                            <a href="{{ route('password.request') }}" class="card-link">Forgot Password?</a></div>
                      </div>
                      <button type="submit" class="btn btn-outline-primary btn-block"> Login</button>
                    </form>
                  </div>
                  <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2">
                    <span>New User</span>
                  </p>
                  <div class="card-body">
                    <a href="{{route('register')}}" class="btn btn-outline-danger btn-block"> Register</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>


@endsection
