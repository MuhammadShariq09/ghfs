<div class="main-menu menu-fixed menu-light menu-accordion" data-scroll-to-active="true">
	<div class="main-menu-content ps-container ps-theme-dark ps-active-y" style="">
		<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
			@if(Auth::user()->type==2)
			<li class="nav-item"><a href="{{url('/')}}"><i class="fa fa-home"></i><span class="menu-title" data-i18n="">Home</span></a></li>      
			<li class="nav-item"><a href="{{url('/admin/users')}}"><i class="fa fa-user"></i><span class="menu-title" data-i18n="">Users</span></a></li>
			<li class="nav-item"><a href="#"><i class="fa fa-th-large"></i><span class="menu-title" data-i18n="">Challenges</span></a></li>
			<li class="nav-item"><a href="{{url('admin/settings') }}"><i class="fa fa-th-large"></i><span class="menu-title" data-i18n="">Settings</span></a></li>
			@elseif(Auth::user()->type == 1)
			<li class="nav-item"><a href="#"><i class="fa fa-home"></i><span class="menu-title" data-i18n="">Home</span></a></li>      
			<li class="nav-item"><a href="#"><i class="fa fa-user"></i><span class="menu-title" data-i18n="">Users</span></a></li>
			<li class="nav-item"><a href="#"><i class="fa fa-th-large"></i><span class="menu-title" data-i18n="">Challenges</span></a></li>			
			@else
			<li class="nav-item"><a href="#"><i class="fa fa-home"></i><span class="menu-title" data-i18n="">Home</span></a></li>      
			<li class="nav-item"><a href="#"><i class="fa fa-user"></i><span class="menu-title" data-i18n="">Users</span></a></li>
			<li class="nav-item"><a href="#"><i class="fa fa-th-large"></i><span class="menu-title" data-i18n="">Challenges</span></a></li>
			@endif
		</ul>
		<div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
			<div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
		</div>
		<div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 274px;">
			<div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 258px;"></div>
		</div>
	</div>
</div>
