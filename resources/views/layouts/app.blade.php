<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ env('APP_NAME') }} - @yield('title')</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" />
    <link href="https://fonts.googleapis.com/css?family=Khand:300,400,500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/vendors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/icheck/icheck.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/icheck/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/login-register.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @yield('css')
</head>
<body class="@yield('body-class')" >


@if(Auth::check())
    @include('inc.header')
    @include('inc.nav')
@endif
@yield('content')


@if(Auth::check())
    <div class="clearfix">
        <input type="hidden" id="user_id" value="{{Auth::user()->id}}">
    </div>
@endif
<script src="{{asset('app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}"
        type="text/javascript"></script>
<script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/js/core/app.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/js/scripts/forms/form-login-register.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/vendors/js/extensions/toastr.min.js')}}" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4&libraries=places&callback=initAutocomplete"
        async defer></script>
<script type="text/javascript">
    var base_url = "{{url('/')}}";
    
    $('.zero-configuration').DataTable({
        "order" : []
    });
    

    $(document).ready(function() {
     $('[data-toggle="popover"]').popover({
        placement: 'top',
        trigger: 'hover'
     });
    });    
    
    $("input[type=number]").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("input[name=phone]").on('change', function (e) {
        if($(this).val().length < 8)
            toastr.error("minimum 8 digit Number", 'Error !');

    });

    $("input[type=email]").on('change', function(){
        validateEmail($(this).val());
    })

    function validateEmail(sEmail) {
        var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

        if(!sEmail.match(reEmail)) {
            toastr.error("Invalid Email Address", 'Error !');
            $("input[type=email]").val("");
            return 1;
        }

        return 0;
    }

    // function getNotifications(){
    //   $.ajax({
    //     url: base_url+'/settings/get-unread-msg',
    //     success: function(response){
    //         $(".notificationpanel").html(response);
    //     }
    //   })
    // }    
    
    // function getProjectsRelatedNotifications(){
    //   $.ajax({
    //     url: base_url+'/settings/get-projects-notifications',
    //     success: function(response){
    //         //console.log(response);
    //         $(".pronotificationpanel").html(response.ret);
    //         $(".pronoticount").text(response.count);
    //     }
    //   })
    // }
    
    // function getProjectsRelatedNotificationsCount(){
    //   $.ajax({
    //     url: base_url+'/settings/get-projects-notifications/1',
    //     success: function(response){
    //         $(".ordernoticount").text(response);
    //     }
    //   })
    // }
    // function getNotificationsCount(){
    //   $.ajax({
    //     url: base_url+'/settings/get-unread-msg/1',
    //     success: function(response){
    //         $(".noticount").text(response);
    //     }
    //   })
    // }


    @if(Auth::check())

    // function getUnreadChatMessagesCount(){
    //   $.ajax({
    //     url: 'https://dev28.onlinetestingserver.com/soachatcentralizedWeb/api/user/get-unread-messages-count/0825a3dec820d1d26501353e0d901f18/{{Auth::user()->id}}',
    //     success: function(response){
    //         $('.chatunreadcount').text('');
    //         if(response.data)
    //             $('.chatunreadcount').text(response.data);
            
    //     }
    //   })
    // }


    // window.addEventListener("message", function(event) {
    //     if(event.data == 'reloadnoticount'){
    //         setTimeout(function(){
    //             getUnreadChatMessagesCount();
    //         },1000);
            
    //     }
    // });

    // $(document).ready(function(){
    //   getUnreadChatMessagesCount();
    //   getNotifications();
    //   getNotificationsCount();
    //   getProjectsRelatedNotifications();
    // });
    

    // setInterval(function(){
    // //   getNotifications();
    // //   getNotificationsCount();
    // //   getProjectsRelatedNotifications();
    // },2000);

    // $(".dropdown-notification a").on('click', function(){
    //   getNotifications();
    //   getNotificationsCount();
    //   getProjectsRelatedNotifications();
    // });
    
    @endif
    

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;
    var componentForm = {
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        console.log(place.geometry.location.lat());
        console.log(place.geometry.location.lng());
        for (var component in componentForm) {
            try{
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }catch(e){

            }
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            console.log(addressType)
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }


</script>

@yield('js')
</body>
</html>
