<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('sendmail', function(){
    $user = [
        'name' => 'Syed M Sohaib',
        'isAdmin' => 1,
    ];
    Mail::to('muhammad.sohaib@salsoft.net')->send(new \App\Mail\WelcomeEmail($user));
});

Route::post('logout', 'HomeController@logout')->name('logout');

Auth::routes(['verify' => true]);
Route::group(['middleware' => ['auth','verified','ReadNoti']], function(){
    
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/', 'HomeController@index');
    Route::get('/profile', 'HomeController@profile')->name('profile');
    Route::post('/update-profile', 'HomeController@updateProfile');
    Route::post('/update-profile-image', 'HomeController@updateProfilePicture');
        

    Route::get('user/send-friend-request', 'AdminController@sendFriendRequest');
    Route::get('user/accept-friend-request', 'AdminController@acceptFriendRequest');

    //Admin Routes
    
    Route::group(['prefix' => 'admin',  'middleware' => 'admin-only'], function(){
        Route::get('/overview', 'HomeController@profile');
        Route::get('/profile', 'HomeController@profile');
        // User Routes
        Route::get('/users/add', 'AdminController@add');
        Route::get('/users/view/{id}', 'AdminController@viewUser');
        Route::get('/users/edit/{id}', 'AdminController@editUser');
        Route::get('/users/inactive/{id}', 'AdminController@inactiveUser');
        Route::get('/users/delete/{id}', 'AdminController@deleteUser');
        Route::get('/users', 'AdminController@allUsers');
        Route::post('/users/add', 'AdminController@add');

        // Settings Routes
        Route::get('/settings','AdminController@settings');
        Route::post('/setting/save','AdminController@saveSettings');
        
        // Challenge Routes
        Route::get('/challenge/add','AdminController@addChallenge');
        

    });
    


    
});



























