<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FitToPath extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'fit_to_path';

}
