<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stats extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'user_stats';

    protected $fillable = [
        'level', 'path', 'points', 'initiate_request', 'challenge_votes', 'accept_request', 'days_remaining', 'user_id'
    ];
    
}
