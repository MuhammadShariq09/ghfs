<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'settings';

    protected $fillable = [
        'key', 'value'
    ];
    
    public static function map(){
        $return =  [];
        foreach(self::all() as $app){
            $return[$app->key] = $app;
        }
        return $return;
    }

}
