<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\FriendableTrait;

class User extends Authenticatable
{
    use Notifiable;
    use \BinaryCabin\LaravelUUID\Traits\HasUUID;
    use FriendableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'address', 'country', 'city', 'state', 'zipcode', 'contact', 'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    protected $uuidFieldName = 'uuid';

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function stats(){
        return $this->hasOne('App\Models\Stats');
    }
 
    public static function findByEmail($email){
        return self::where('email',$email)->first();
    }
        
    public static function getUsersById($ids){
        return self::whereIn('id',$ids)->get();
    }
        
    public static function findAdmins(){
        return self::where('type',2)->get();
    }


}
