<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use \App\Mail\WelcomeEmail;
use Validator;
use Mail;
use App\User;
use Helper;
use Hash;
use File;
use App\Models\Settings;
use App\Models\Stats;
use App\Models\FitToPath;
use App\Models\Categories;
use DB;

class AdminController extends Controller
{
    public function allUsers(Request $request){
        $users = User::with('stats')->where('type',0)->get();
        return view('pages.users.all',compact('users'));
    }

    public function viewUser(Request $request, $uuid){
        $user = User::with('stats')->byUUID($uuid)->first();

        if($request->query('friends')){
            $friends = $user->getFriends()->load('stats');
            return view('pages.users.friends',compact('user','friends'));
        }

        return view('pages.users.view',compact('user'));
    }

    public function sendFriendRequest(Request $request){
        $user   = User::where('type',0)->where('id','=',7)->first();
        $record = User::where('type',0)->where('id','!=',7)->get();
        foreach($record as $recipient){            
            $user->befriend($recipient);
        }
    }

    public function acceptFriendRequest(Request $request){
        $user1     = User::find(8);
        $user2     = User::find(7);
        $user1->acceptFriendRequest($user2);
    }

    public function add(Request $request){
        $settings = Settings::map();

        if(!$request->isMethod('post')){
            return view('pages.users.add');
        }else{
            
            if ($request->file('personal_img')) {
                $validator = Validator::make(
                    array('fileValidation' => $request->file('personal_img')),
                    array('fileValidation' => 'required|mimes:png,jpg,jpeg,bmp')
                );
                if ($validator->fails()) {
                    Session::flash('error', 'File type is not supported');
                    return redirect()->back()->withInput();
                }
            }
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'min:8'],
                //'password' => ['required', 'regex:/^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z0-9]+$/','min:8'],
                'telephone' => ['number', 'min:6'],
            ],['password.regex' => 'Password should be minimum 8 characters in length with atleast One Number and Alphabet']); 
            
            if ($validator->fails()) {
                Session::flash('error', $validator->messages()->first());
                return redirect()->back();
            }

            if($request->phone != '' && strlen($request->phone) < 8){
                Session::flash('error', 'Minimum phone number length can be 8 character');
                return redirect()->back();            
            }
            $inputs = [
                'name' => $request->name ,
                'contact' => $request->phone ,
                'email' => $request->email ,
                'gender' => $request->gender ,
                'country' => $request->country ,
                'city' => $request->city ,
                'dob' => $request->dob ,
                'state' => $request->state ,
                'zipcode' => $request->zip_code ,
                'address' => $request->address ,
                'created_at' => Helper::datetimeStamp(),
                'password' => Hash::make($request->password),
            ];
            
            //$user_id = DB::table('users')->insertGetId($inputs);
            $user = User::create($inputs);
            
            $user_id = $user->id;

            Stats::create([
                'level' => config('initialLevel'),
                'path' => config('initialPath'),
                'points' => 0,
                'initiate_request' => $settings['initiate_request_per_user']->value,
                'challenge_votes' => $settings['no_of_votes_per_user']->value,
                'accept_request' => $settings['challenge_acpt_rate_per_user']->value,
                'days_remaining' => $settings['reset_date_per_user']->value,
                'user_id' => $user_id
            ]);

            // $user->sendEmailVerificationNotification();

            if($request->personal_img_base64){
                $image64 = $request->personal_img_base64;
                if(!empty($image64) || $image64 != ''){
                list($type, $image64) = explode(';', $image64);
                list(,$extension) = explode('/',$type);
                list(, $image64)      = explode(',', $image64);
                }
            
                $directoryPath = public_path().'/assets/users/' . md5($user_id);
                
                if(File::isDirectory($directoryPath)){
                } else {
                    File::makeDirectory($directoryPath);
                }
        
                $image64 = base64_decode($image64);
                $time = time();
                $path = '/assets/users/'.md5($user_id).'/';
                $destination = public_path($path);
                $filename = md5(rand(11111111,99999999)).'.png';
                file_put_contents($destination.$filename, $image64);
                User::where('id', $user_id)
                    ->update(['image' => $path.$filename]);  
                
            }
                
            /*  Send welcome email to user and admin */

            Mail::to($request->email)->send(new WelcomeEmail($inputs));

            Session::flash('message','User is inserted');
            return redirect('/admin/users');
        }
    }

    public function settings(Request $request){
        $settings = Settings::all();
        return view('pages.settings.all',compact('settings'));
    }


    public function saveSettings(Request $request){
        $array = [];
        foreach($request->all() as $index => $value):
            if(!$value){
                Session::flash('error','All Fields must be filled');
                return redirect()->back();                
            }else
                $array[$index] = $value;
        endforeach;
        unset($array['_token']);
        DB::transaction(function () use ($array) {
            foreach($array as $index => $arr):
                Settings::where('key',$index)->update(['value' => $arr]);
            endforeach;
        },1);
        DB::commit();
        Session::flash('message','Settings are updated');
        return redirect()->back();
    }

    public function addChallenge(Request $request){
        $FitToPath = FitToPath::all();
        $Categories = Categories::all();
        return view('pages.challenges.add', compact('FitToPath','Categories'));
    }

}

