<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Helper;

class ReadNoti
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && $request->query('notification_id'))
            Auth::user()->unreadNotifications()->where('id',$request->query('notification_id'))->update(['read_at' => Helper::datetimeStamp()]);
            
        return $next($request);
    }
}
