<?php

namespace App;
use DB;
use Session;
use DateTime;
use DateTimeZone;
use Auth;
use Crypt;
use App\User;

class Helper
{

    public static function year_list($name, $selected_id='', $extra=''){
        $from = date('Y')-10;
        $to = date('Y');
        $yearArray = range($from, $to);
        $selected='';
        echo '<select '.$extra.' name="'.$name.'" id="'.$name.'"  class="">';
        if(!$selected_id)
            $selected_id = date('Y');
        echo '<option value="">Select Year</option>';
        foreach ($yearArray as $year) {
            if($selected_id)
                $selected = ($year == $selected_id) ? 'selected' : '';
            echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
        }
        echo '</select>';        
    }
    
    public static function userslist($name, $selected_id='', $extra=''){
        $data = DB::table('users')->where('deleted',0)->where('type',0)->orderBy('id','desc')->get();
        echo '<select '.$extra.' name="'.$name.'" id="'.$name.'" >';
            echo '<option value="">Select Users</option>';
            foreach($data as $d){
                $sel = ($d->id == $selected_id) ? "selected" : "";
                echo '<option '.$sel.' value="'.$d->id.'">'.$d->name.'</option>';
            }
        echo '</select>';
    }    
    
    public static function month_list($name, $selected_id='', $extra=''){
        $formattedMonthArray = array(
                    "1" => "January", "2" => "February", "3" => "March", "4" => "April",
                    "5" => "May", "6" => "June", "7" => "July", "8" => "August",
                    "9" => "September", "10" => "October", "11" => "November", "12" => "December",
        );
        $selected='';
        echo '<select '.$extra.' name="'.$name.'" id="'.$name.'"  class="">';
        echo '<option value="">Select</option>';
        foreach ($monthArray as $month) {
            if($selected_id)
                $selected = ($month == $selected_id) ? 'selected' : '';
            // if you want to add extra 0 before the month uncomment th e line below
            //$month = str_pad($month, 2, "0", STR_PAD_LEFT);
            echo '<option  '.$selected.' value="'.$month.'">'.$formattedMonthArray[$month].'</option>';
        }
        echo '</select>';        
    }
        
    /* User list */
    public static function getUsersList($name, $selected_id='', $except='', $show_email=''){
        $data = DB::table('users')->where('deleted',0)->get();
        if($except)
            $data = DB::table('users')->where('deleted',0)->where('id','!=',$except)->get();
        echo '<select name="'.$name.'" id="'.$name.'"  class="form-control">';
            echo '<option>Select</option>';
            foreach($data as $d){
                $sel = ($d->id == $selected_id) ? "selected" : "";
                if($show_email)
                    echo '<option data-email="'.Crypt::encryptString($d->email).'" '.$sel.' value="'.$d->id.'">'.$d->name.'</option>';
                else
                    echo '<option '.$sel.' value="'.$d->id.'">'.$d->name.'</option>';
            }
        echo '</select>';
    }


    public static function random_color(){
       $colors =  array('#e5213b','#fd612c','#fd9a00','#a4cf30','#20aaea','#7a6ff0','#aa62e3','#e362e3','#8da3a6','#37c5ab','#eec300');
       return $colors[array_rand($colors,1)];
    }

    public static function getInitials($name, $img=''){
        $color = substr(self::random_color(),1);
        if(!$img){
            return 'https://ui-avatars.com/api/?rounded=true&background='.$color.'&color=fff&name='.$name;
        }
        else{
            return asset($img);
        }
    }

    public static function getInitialsByField($field=null){
        $user = DB::table('users')->where('id',$field)->orWhere('email',$field)->get();
        if(count($user)){
            $color = substr(self::random_color(),1);
            if(!$user[0]->image){
                return 'https://ui-avatars.com/api/?rounded=true&background='.$color.'&color=fff&name='.$user[0]->name;
            }
            else{
                return asset($user[0]->image);
            }
        }
        return asset('avatar.jpg');
    }


    public static function file_url_contents($url){
        if (!function_exists('curl_init')){
            die('CURL is not installed!');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public static function datetimeStamp($convertDate='',$convertToZone='',$converFromZone='', $convertDateFormat="Y-m-d h:i:s A", $returnDateFormat="Y-m-d h:i:s A"){
        $res = '';
        if(!$convertDate)
            $convertDate = date("Y-m-d h:i:s A");

        if($convertToZone==''){
            $whitelist = array(
                '127.0.0.1',
                '::1'
            );

            if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
                $ipAddress = self::getRealIpAddr();
            }
            else
            {
                $ipAddress = self::file_url_contents('https://api.ipify.org/');
            }

            $context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
            try{
                //echo "https://ipapi.co/$ipAddress/timezone/";
                $ipInfo = self::file_url_contents("https://ipapi.co/$ipAddress/timezone/",false,$context);
            }catch(\Exception $e){
                return date("Y-m-d h:i:s A");
            }
            $convertToZone = $ipInfo;
        }
        if($converFromZone==''){
            $converFromZone = date_default_timezone_get();
        }

        if(!in_array($converFromZone, timezone_identifiers_list())) { // check source timezone
           return date("Y-m-d h:i:s A");
            //trigger_error(__FUNCTION__ . ': Invalid source timezone ' . $converFromZone, E_USER_ERROR);
        } elseif(!in_array($convertToZone, timezone_identifiers_list())) { // check destination timezone
            //trigger_error(__FUNCTION__ . ': Invalid destination timezone ' . $convertToZone, E_USER_ERROR);
               return date("Y-m-d h:i:s A");
        } else {
            // create DateTime object
            $d = DateTime::createFromFormat($convertDateFormat, $convertDate, new DateTimeZone($converFromZone));
            // check source datetime
            if($d && DateTime::getLastErrors()["warning_count"] == 0 && DateTime::getLastErrors()["error_count"] == 0) {
                // convert timezone
                $d->setTimeZone(new DateTimeZone($convertToZone));
                // convert dateformat
                $res = $d->format($returnDateFormat);
            } else {
                   return date("Y-m-d h:i:s A");
                   trigger_error(__FUNCTION__ . ': Invalid source datetime ' . $convertDate . ', ' . $convertDateFormat, E_USER_ERROR);
            }
        }
        return $res;
    }

    public static function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}
